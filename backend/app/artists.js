const express = require('express');
const Artist = require('../models/Artist');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const config = require('../config');
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const createRouter = () => {
    const router = express.Router();

    router.get('/', (req, res) => {
        Artist.find()
            .then(artists => res.send(artists))
            .catch(() => res.sendStatus(500));
    });

    router.post('/', [auth, permit('user', 'admin')], upload.single('image'), (req, res) => {
        const artistData = req.body;
        if (req.file) {
            artistData.image = req.file.filename;
        } else {
            artistData.image = null;
        }

        const artist = new Artist(artistData);

        artist.save()
            .then(result => res.send(result))
            .catch(error => res.status(400).send(error));
    });

    router.delete('/:id', [auth, permit('admin')], async (req, res) => {

        await Artist.deleteOne({_id: req.params.id})
            .then(result => res.send(result))
            .catch(error => res.status(400).send(error))
    });

    router.put('/:id', [auth, permit('admin')], async (req, res) => {
        const artistData = await Artist.findOne({_id: req.params.id});

        artistData.published = true;

        artistData.save()
            .then(artists => res.send(artists))
            .catch(error => res.status(400).send(error));
    });

    return router;
};

module.exports = createRouter;