const express = require('express');
const User = require('../models/User');
const TrackHistory = require('../models/TrackHistory');
const Album = require('../models/Album');

const createRouter = () => {
    const router = express.Router();

    router.get('/', (req, res) => {
        const id = req.query.userID;

        TrackHistory.find({userID: id}).populate('trackID userID')
            .then(response => {
                const promiseArray = [];
                    response.forEach(history => {
                        promiseArray.push(new Promise((resolve, reject) => {
                            Album.findOne({_id: history.trackID.album}).populate('artist')
                                .then(result => {
                                    resolve({history, artist: result.artist.name});
                                });
                        }))
                    });
                Promise.all(promiseArray).then(result => res.send(result.reverse()));
            }).catch(error => res.status(401).send(error));
    });

    router.post('/', async (req, res) => {
        const token = req.get('Token');
        const historyData = req.body;

        if (!token) res.status(401).send({error: 'Token is not present!'});

        const user = await User.findOne({token: token});

        if (!user) res.status(401).send({error: 'User not found!'});

        historyData.userID = user._id;
        historyData.dateTime = new Date().toISOString();
        const history = new TrackHistory(historyData);

        await history.save();

        return res.send(history);
    });

    return router;
};

module.exports = createRouter;

