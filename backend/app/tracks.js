const express = require('express');
const Track = require('../models/Track');
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');

const createRouter = () => {
    const router = express.Router();

    router.get('/', (req, res) => {
        const id = req.query.album;

        if (id) {
            Track.find({album: id}).populate('album')
                .then(result => {
                    const withTrackNumber = result.map((track, key) => {
                        return {track, number: key + 1}
                    });
                    res.send(withTrackNumber);
                })
                .catch(() => res.sendStatus(500));
        } else {
            Track.find().populate('album')
                .then(results => res.send(results))
                .catch(() => res.sendStatus(500));
        }
    });

    router.post('/', [auth, permit('user', 'admin')], (req, res) => {
        const trackData = req.body;
        const track = new Track(trackData);

        track.save()
            .then(result => res.send(result))
            .catch(error => res.status(400).send(error));
    });

    router.get('/:id', (req, res) => {
        const id = req.params.id;
        Track.find().populate('album').then(results => {
            const tracks = results.filter(result => {
                const artistID = result.album.artist.toString();
                if (artistID === id) return result;
            });
            res.send(tracks);
        });
    });

    router.delete('/:id', [auth, permit('admin')], async (req, res) => {

        await Track.deleteOne({_id: req.params.id})
            .then(result => res.send(result))
            .catch(error => res.status(400).send(error))
    });

    router.put('/:id', [auth, permit('admin')], async (req, res) => {
        const trackData = await Track.findOne({_id: req.params.id});

        trackData.published = true;

        trackData.save()
            .then(artists => res.send(artists))
            .catch(error => res.status(400).send(error));
    });

    return router;
};

module.exports = createRouter;