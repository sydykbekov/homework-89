const express = require('express');
const Album = require('../models/Album');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const config = require('../config');
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const createRouter = () => {
    const router = express.Router();

    router.get('/', (req, res) => {

        const id = req.query.artist;

        if (id) {
            Album.find({artist: id}).populate('artist')
                .then(albums => res.send(albums))
                .catch(() => res.sendStatus(500));
        } else {
            Album.find().populate('artist')
                .then(albums => res.send(albums))
                .catch(() => res.sendStatus(500));
        }
    });

    router.post('/', [auth, permit('user', 'admin')], upload.single('image'), (req, res) => {
        const albumData = req.body;

        if (req.file) {
            albumData.image = req.file.filename;
        } else {
            albumData.image = null;
        }

        const album = new Album(albumData);

        album.save()
            .then(result => res.send(result))
            .catch(error => res.status(400).send(error));
    });

    router.get('/:id', (req, res) => {

        const id = req.params.id;

        Album.findOne({_id: id}).populate('artist')
            .then(albums => res.send(albums))
            .catch(() => res.sendStatus(500));
    });

    router.delete('/:id', [auth, permit('admin')], async (req, res) => {

        await Album.deleteOne({_id: req.params.id})
            .then(result => res.send(result))
            .catch(error => res.status(400).send(error))
    });

    router.put('/:id', [auth, permit('admin')], async (req, res) => {
        const albumData = await Album.findOne({_id: req.params.id});

        albumData.published = true;

        albumData.save()
            .then(artists => res.send(artists))
            .catch(error => res.status(400).send(error));
    });

    return router;
};

module.exports = createRouter;