const mongoose = require('mongoose');
const config = require('./config');

const Artist = require('./models/Artist');
const Album = require('./models/Album');
const Track = require('./models/Track');
const User = require('./models/User');

mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;

db.once('open', async () => {
    try {
        await db.dropCollection('users');
        await db.dropCollection('artists');
        await db.dropCollection('albums');
        await db.dropCollection('tracks');
        await db.dropCollection('trackhistories');
    } catch (e) {
        console.log('Collections were not present, skipping drop...');
    }

    await User.create({
        username: 'Mike',
        password: '123',
        role: 'user'
    }, {
        username: 'Carl',
        password: '123',
        role: 'admin'
    });

    const [Krid, Rammstein] = await Artist.create({
        name: 'Егор Крид',
        info: 'Егор Крид — российский певец.',
        image: 'Krid.jpg',
        published: true
    }, {
        name: 'Rammstein',
        info: 'Rammstein — культовая немецкая рок-группа, образованная в январе 1994 года в Берлине. Музыкальный стиль группы относится к жанру индастриал-метала.',
        image: 'Rammstein.jpg',
        published: false
    });

    const [holostyak, chto_oni_znaut, reise] = await Album.create({
        name: 'Холостяк',
        artist: Krid._id,
        year: '2015',
        image: 'holostyak.jpg',
        published: true
    }, {
        name: 'Что они знают?',
        artist: Krid._id,
        year: '2017',
        image: 'chto_oni_znaut.jpg',
        published: false
    }, {
        name: 'Rammstein',
        artist: Rammstein._id,
        year: '2004',
        image: 'reise.jpg',
        published: false
    });

    await Track.create({
        name: 'Reise, Reise',
        album: reise._id,
        duration: '4:11',
        published: false
    }, {
        name: 'Mein Teil',
        album: reise._id,
        duration: '4:33',
        published: true
    }, {
        name: 'Keine Lust',
        album: reise._id,
        duration: '3:43',
        published: false
    }, {
        name: 'LOS',
        album: reise._id,
        duration: '4:24',
        published: true
    }, {
        name: 'DALAI LAMA',
        album: reise._id,
        duration: '5:39',
        published: true
    }, {
        name: 'Интро',
        album: chto_oni_znaut._id,
        duration: '2:41',
        published: true
    }, {
        name: 'Что они знают?',
        album: chto_oni_znaut._id,
        duration: '2:56',
        published: true
    }, {
        name: 'Не могу',
        album: chto_oni_znaut._id,
        duration: '3:18',
        published: true
    }, {
        name: 'Потрачу',
        album: chto_oni_znaut._id,
        duration: '3:04',
        published: true
    }, {
        name: 'Самба белого мотылька',
        album: chto_oni_znaut._id,
        duration: '3:06',
        published: true
    }, {
        name: 'Самая самая',
        album: holostyak._id,
        duration: '3:51',
        published: true
    }, {
        name: 'Закрой глаза',
        album: holostyak._id,
        duration: '3:51',
        published: false
    }, {
        name: 'Надо ли',
        album: holostyak._id,
        duration: '3:20',
        published: true
    }, {
        name: 'Ревность',
        album: holostyak._id,
        duration: '3:18',
        published: false
    }, {
        name: 'Невеста',
        album: holostyak._id,
        duration: '3:26',
        published: true
    });

    db.close();
});