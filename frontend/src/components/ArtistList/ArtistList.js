import React from 'react';
import notFound from '../../assets/images/not-found.png';
import {Button, Image, Panel} from "react-bootstrap";
import {Link} from "react-router-dom";

const ArtistList = props => {
    let image = notFound;

    if (props.image) {
        image = 'http://localhost:8000/uploads/' + props.image;
    }

    return (
        <Panel>
            <Panel.Body>
                <Image
                    style={{width: '100px', marginRight: '10px'}}
                    src={image}
                    thumbnail
                />
                <Link to={'/artist/' + props.id}>
                    {props.name}
                </Link>
                {props.notPublished && <span> ({props.notPublished})</span>}
                {props.remove && <Button className="pull-right" bsStyle="danger" onClick={() => props.remove(props.id)}>Удалить</Button>}
                {props.notPublished && <Button className="pull-right" bsStyle="primary" onClick={() => props.update(props.id)}>Опубликовать</Button>}
            </Panel.Body>
        </Panel>
    )
};

export default ArtistList;
