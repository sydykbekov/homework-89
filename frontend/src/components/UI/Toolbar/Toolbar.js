import React from 'react';
import {Nav, Navbar, NavItem} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";
import AnonymousMenu from "./Menus/AnonymousMenu";
import UserMenu from "./Menus/UserMenu";
import Adding from "./Menus/Adding";

const Toolbar = ({user, logout}) => (
    <Navbar>
        <Navbar.Header>
            <Navbar.Brand>
                <LinkContainer to="/" exact><a>Last.fm</a></LinkContainer>
            </Navbar.Brand>
            <Navbar.Toggle />
        </Navbar.Header>
        <Navbar.Collapse>
            <Nav>
                <LinkContainer to="/" exact>
                    <NavItem>Artists</NavItem>
                </LinkContainer>
            </Nav>
            {user ? <UserMenu user={user} logout={logout} /> : <AnonymousMenu/>}
            {user && <Adding/>}
        </Navbar.Collapse>
    </Navbar>
);

export default Toolbar;