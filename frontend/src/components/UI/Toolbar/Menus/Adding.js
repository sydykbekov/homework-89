import React from 'react';
import {MenuItem, Nav, NavDropdown} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";

const Adding = () => {
    return (
        <Nav pullRight>
            <NavDropdown title="Добавить" id="adding">
                <LinkContainer to="/add-artist" exact>
                    <MenuItem>Артиста</MenuItem>
                </LinkContainer>
                <LinkContainer to="/add-album" exact>
                    <MenuItem>Альбом</MenuItem>
                </LinkContainer>
                <LinkContainer to="/add-track" exact>
                    <MenuItem>Трэк</MenuItem>
                </LinkContainer>
            </NavDropdown>
        </Nav>
    )
};

export default Adding;