import React, {Fragment} from 'react';
import {MenuItem, Nav, NavDropdown} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";

const UserMenu = ({user, logout}) => {
    const navTitle = (
        <Fragment>
            Привет, <b>{user.username}</b>!
        </Fragment>
    );

    return (
        <Nav pullRight>
            <NavDropdown title={navTitle} id="user-menu">
                <LinkContainer to="/history" exact>
                    <MenuItem>История</MenuItem>
                </LinkContainer>
                <MenuItem divider/>
                <MenuItem onClick={logout}>Выйти</MenuItem>
            </NavDropdown>
        </Nav>
    )
};

export default UserMenu;
