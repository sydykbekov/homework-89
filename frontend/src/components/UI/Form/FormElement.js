import React from 'react';
import {Col, ControlLabel, FormControl, FormGroup, HelpBlock} from "react-bootstrap";
import PropTypes from 'prop-types';

const FormElement = props => {
    let componentClass,
        formControlChildren;

    if (props.type === 'select') {
        componentClass = 'select';

        formControlChildren = props.options.map(element => (
            <option key={element._id} value={element._id}>{element.name}</option>
        ));
        formControlChildren = [<option key="empty">{'Please select ' + props.title + ' ...'}</option>, ...formControlChildren];
    }
    return (
        <FormGroup
            controlId={props.propertyName}
            validationState={props.error && 'error'}>
            <Col componentClass={ControlLabel} sm={2}>
                {props.title}
            </Col>

            <Col sm={10}>
                <FormControl
                    type={props.type}
                    componentClass={componentClass}
                    required={props.required}
                    placeholder={props.placeholder}
                    name={props.propertyName}
                    value={props.value}
                    onChange={props.changeHandler}
                    autoComplete={props.autoComplete}
                >
                    {formControlChildren}
                </FormControl>
                {props.error &&
                <HelpBlock>{props.error}</HelpBlock>
                }
            </Col>
        </FormGroup>
    )
};

FormElement.propTypes = {
    propertyName: PropTypes.string.isRequired,
    error: PropTypes.string,
    title: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    changeHandler: PropTypes.func.isRequired,
    required: PropTypes.bool,
    autoComplete: PropTypes.string
};

export default FormElement;