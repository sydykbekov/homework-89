import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {Button, PageHeader, Panel} from "react-bootstrap";
import {getTracks, removeTrack, sendHistory, updateTrack} from "../../store/actions/artists";

class Tracks extends Component {
    componentDidMount() {
        this.props.getTracks(this.props.match.params.albumID);
    }

    send = (trackID) => {
        if (this.props.user) {
            const info = {
                userID: this.props.user._id,
                trackID: trackID
            };

            this.props.sendHistory(info, this.props.user.token);
        }
    };

    render() {
        let trackList;
        if (this.props.user && this.props.user.role === 'admin') {
            trackList = this.props.tracks.map(track => (
                <Panel key={track.track._id}>
                    <Panel.Body>
                        <p className="track"
                           onClick={() => this.send(track.track._id)}>{track.number}. {track.track.name} {`(${track.track.duration})`}</p>
                        {!track.track.published && <span> (не опубликовано)</span>}
                        {<Button className="pull-right" bsStyle="danger"
                                 onClick={() => this.props.removeTrack(track.track._id, this.props.user.token, this.props.match.params.albumID)}>Удалить</Button>}
                        {!track.track.published && <Button className="pull-right" bsStyle="primary"
                                                     onClick={() => this.props.updateTrack(track.track._id, this.props.user.token, this.props.match.params.albumID)}>Опубликовать</Button>}
                    </Panel.Body>
                </Panel>
            ))
        } else {
            trackList = this.props.tracks.map(track => (
                track.track.published &&
                <Panel key={track.track._id}>
                    <Panel.Body>
                        <p className="track"
                           onClick={() => this.send(track.track._id)}>{track.number}. {track.track.name} {`(${track.track.duration})`}</p>
                    </Panel.Body>
                </Panel>
            ))
        }
        return (
            <Fragment>
                <PageHeader>
                    Треки
                </PageHeader>
                {trackList}
            </Fragment>
        )
    }
}

const mapStateToProps = state => ({
    tracks: state.artists.tracks,
    user: state.users.user
});

const mapDispatchToProps = dispatch => ({
    getTracks: id => dispatch(getTracks(id)),
    sendHistory: (listenedTrack, token) => dispatch(sendHistory(listenedTrack, token)),
    removeTrack: (id, token, albumID) => dispatch(removeTrack(id, token, albumID)),
    updateTrack: (id, token, albumID) => dispatch(updateTrack(id, token, albumID))
});


export default connect(mapStateToProps, mapDispatchToProps)(Tracks);