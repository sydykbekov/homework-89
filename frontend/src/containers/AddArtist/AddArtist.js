import React, {Component, Fragment} from 'react';
import {Button, Col, Form, FormGroup, PageHeader} from "react-bootstrap";
import FormElement from "../../components/UI/Form/FormElement";
import {connect} from "react-redux";
import {addArtist} from "../../store/actions/artists";

class AddArtist extends Component {
    state = {
        name: '',
        info :'',
        image: ''
    };

    inputChangeHandler = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    submitFormHandler = event => {
        event.preventDefault();

        const formData = new FormData();
        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key])
        });

        this.props.onSubmit(formData, this.props.user.token);
    };

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        })
    };


    render() {
        return (
            <Fragment>
                <PageHeader>Добавить артиста</PageHeader>
                <Form horizontal onSubmit={this.submitFormHandler}>
                    <FormElement
                        propertyName="name"
                        title="Name"
                        placeholder="Enter artist name"
                        autoComplete="new-artist-name"
                        type="text"
                        value={this.state.name}
                        changeHandler={this.inputChangeHandler}
                    />

                    <FormElement
                        propertyName="info"
                        title="Info"
                        placeholder="About artist"
                        autoComplete="new-info"
                        type="text"
                        value={this.state.info}
                        changeHandler={this.inputChangeHandler}
                    />

                    <FormElement
                        propertyName="image"
                        title="Image"
                        type="file"
                        changeHandler={this.fileChangeHandler}
                    />

                    <FormGroup>
                        <Col smOffset={2} sm={10}>
                            <Button
                                bsStyle="primary"
                                type="submit"
                            >Добавить</Button>
                        </Col>
                    </FormGroup>
                </Form>
            </Fragment>
        )
    }
}

const mapStateToProps = state => ({
    user: state.users.user
});

const mapDispatchToProps = dispatch => ({
    onSubmit: (formData, token) => dispatch(addArtist(formData, token))
});

export default connect(mapStateToProps, mapDispatchToProps)(AddArtist);