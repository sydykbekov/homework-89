import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {Button, Image, PageHeader, Panel} from "react-bootstrap";
import {getAlbums, removeAlbum, updateAlbum} from "../../store/actions/artists";
import {Link} from "react-router-dom";

class Albums extends Component {
    componentDidMount() {
        this.props.getAlbums(this.props.match.params.id);
    }

    render() {
        let albumsList;
        if (this.props.user && this.props.user.role === 'admin') {
            albumsList = this.props.albums.map(album => (
                <Panel key={album._id}>
                    <Panel.Body>
                        <Image
                            style={{width: '100px', marginRight: '10px'}}
                            src={'http://localhost:8000/uploads/' + album.image}
                            thumbnail
                        />
                        <Link to={this.props.history.location.pathname + '/album/' + album._id}>
                            {album.name} {`(${album.year})`}
                        </Link>
                        {!album.published && <span> (не опубликовано)</span>}
                        {<Button className="pull-right" bsStyle="danger"
                                 onClick={() => this.props.removeAlbum(album._id, this.props.user.token, this.props.match.params.id)}>Удалить</Button>}
                        {!album.published && <Button className="pull-right" bsStyle="primary"
                                                       onClick={() => this.props.updateAlbum(album._id, this.props.user.token, this.props.match.params.id)}>Опубликовать</Button>}
                    </Panel.Body>
                </Panel>
            ))
        } else {
            albumsList = this.props.albums.map(album => (
                album.published && <Panel key={album._id}>
                    <Panel.Body>
                        <Image
                            style={{width: '100px', marginRight: '10px'}}
                            src={'http://localhost:8000/uploads/' + album.image}
                            thumbnail
                        />
                        <Link to={this.props.history.location.pathname + '/album/' + album._id}>
                            {album.name} {`(${album.year})`}
                        </Link>
                    </Panel.Body>
                </Panel>
            ))
        }
        return (
            <Fragment>
                <PageHeader>
                    Альбомы
                </PageHeader>
                {albumsList}
            </Fragment>
        )
    }
}

const mapStateToProps = state => ({
    user: state.users.user,
    albums: state.artists.albums
});

const mapDispatchToProps = dispatch => ({
    getAlbums: id => dispatch(getAlbums(id)),
    removeAlbum: (id, token, artistID) => dispatch(removeAlbum(id, token, artistID)),
    updateAlbum: (id, token, artistID) => dispatch(updateAlbum(id, token, artistID))
});


export default connect(mapStateToProps, mapDispatchToProps)(Albums);