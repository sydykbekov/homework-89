import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import ArtistList from "../../components/ArtistList/ArtistList";
import {PageHeader} from "react-bootstrap";
import {getArtists, removeArtist, updateArtist} from "../../store/actions/artists";

class Artists extends Component {
    componentDidMount() {
        this.props.getArtists();
    }

    render() {
        let artistList;
        if (this.props.user && this.props.user.role === 'admin') {
            artistList = this.props.artists.map(artist => (
                <ArtistList key={artist._id} id={artist._id} name={artist.name} image={artist.image}
                            remove={() => this.props.removeArtist(artist._id, this.props.user.token)}
                            update={() => this.props.updateArtist(artist._id, this.props.user.token)}
                            notPublished={!artist.published && 'не опубликовано'}/>
            ))
        } else {
            artistList = this.props.artists.map(artist => (
                artist.published ?
                    <ArtistList key={artist._id} id={artist._id} name={artist.name} image={artist.image}/> : null
            ))
        }
        return (
            <Fragment>
                <PageHeader>
                    Исполнители
                </PageHeader>
                {artistList}
            </Fragment>
        )
    }
}

const mapStateToProps = state => ({
    user: state.users.user,
    artists: state.artists.artists
});

const mapDispatchToProps = dispatch => ({
    getArtists: () => dispatch(getArtists()),
    removeArtist: (id, token) => dispatch(removeArtist(id, token)),
    updateArtist: (id, token) => dispatch(updateArtist(id, token))
});

export default connect(mapStateToProps, mapDispatchToProps)(Artists);
