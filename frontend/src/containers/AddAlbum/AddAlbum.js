import React, {Component, Fragment} from 'react';
import {Button, Col, Form, FormGroup, PageHeader} from "react-bootstrap";
import FormElement from "../../components/UI/Form/FormElement";
import {connect} from "react-redux";
import {addAlbum, getArtists} from "../../store/actions/artists";

class AddAlbum extends Component {
    state = {
        name: '',
        artist: '',
        year: '',
        image: ''
    };

    componentDidMount() {
        this.props.getArtists();
    }

    inputChangeHandler = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    submitFormHandler = event => {
        event.preventDefault();

        const formData = new FormData();
        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key])
        });

        this.props.onSubmit(formData, this.props.user.token);
    };

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        })
    };

    render() {
        return (
            <Fragment>
                <PageHeader>Добавить альбом</PageHeader>
                <Form horizontal onSubmit={this.submitFormHandler}>
                    <FormElement
                        propertyName="name"
                        title="Name"
                        placeholder="Enter album name"
                        autoComplete="new-album-name"
                        type="text"
                        value={this.state.name}
                        changeHandler={this.inputChangeHandler}
                    />

                    <FormElement
                        propertyName="artist"
                        title="Artist"
                        type="select"
                        options={this.props.artists}
                        value={this.state.artist}
                        changeHandler={this.inputChangeHandler}
                    />

                    <FormElement
                        propertyName="year"
                        title="Year"
                        placeholder="Enter year"
                        autoComplete="new-year"
                        type="text"
                        value={this.state.year}
                        changeHandler={this.inputChangeHandler}
                    />

                    <FormElement
                        propertyName="image"
                        title="Image"
                        type="file"
                        changeHandler={this.fileChangeHandler}
                    />

                    <FormGroup>
                        <Col smOffset={2} sm={10}>
                            <Button
                                bsStyle="primary"
                                type="submit"
                            >Добавить</Button>
                        </Col>
                    </FormGroup>
                </Form>
            </Fragment>
        )
    }
}

const mapStateToProps = state => ({
    user: state.users.user,
    artists: state.artists.artists
});

const mapDispatchToProps = dispatch => ({
    getArtists: () => dispatch(getArtists()),
    onSubmit: (formData, token) => dispatch(addAlbum(formData, token))
});

export default connect(mapStateToProps, mapDispatchToProps)(AddAlbum);