import React, {Component, Fragment} from 'react';
import {Button, Col, Form, FormGroup, PageHeader} from "react-bootstrap";
import FormElement from "../../components/UI/Form/FormElement";
import {connect} from "react-redux";
import {addTrack, getAllAlbums} from "../../store/actions/artists";

class AddTrack extends Component {
    state = {
        name: '',
        album :'',
        duration: ''
    };

    componentDidMount() {
        this.props.getAlbums();
    }

    inputChangeHandler = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    submitFormHandler = event => {
        event.preventDefault();

        this.props.onSubmit(this.state, this.props.user.token);
    };

    render() {
        return (
            <Fragment>
                <PageHeader>Добавить трек</PageHeader>
                <Form horizontal onSubmit={this.submitFormHandler}>
                    <FormElement
                        propertyName="name"
                        title="Name"
                        placeholder="Enter track name"
                        autoComplete="new-track-name"
                        type="text"
                        value={this.state.name}
                        changeHandler={this.inputChangeHandler}
                    />

                    <FormElement
                        propertyName="album"
                        title="Album"
                        type="select"
                        options={this.props.albums}
                        value={this.state.album}
                        changeHandler={this.inputChangeHandler}
                    />

                    <FormElement
                        propertyName="duration"
                        title="Duration"
                        placeholder="Duration ..."
                        autoComplete="new-duration"
                        type="text"
                        value={this.state.duration}
                        changeHandler={this.inputChangeHandler}
                    />

                    <FormGroup>
                        <Col smOffset={2} sm={10}>
                            <Button
                                bsStyle="primary"
                                type="submit"
                            >Добавить</Button>
                        </Col>
                    </FormGroup>
                </Form>
            </Fragment>
        )
    }
}

const mapStateToProps = state => ({
    user: state.users.user,
    albums: state.artists.albums
});

const mapDispatchToProps = dispatch => ({
    getAlbums: () => dispatch(getAllAlbums()),
    onSubmit: (formData, token) => dispatch(addTrack(formData, token))
});

export default connect(mapStateToProps, mapDispatchToProps)(AddTrack);