import {
    SUCCESS_FETCH_ALBUMS, SUCCESS_FETCH_HISTORY, SUCCESS_FETCH_TRACKS,
    SUCCESS_REQUEST
} from "../actions/artists";

const initialState = {
    artists: [],
    albums: [],
    tracks: [],
    history: []
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case SUCCESS_REQUEST:
            return {...state, artists: action.artists};
        case SUCCESS_FETCH_ALBUMS:
            return {...state, albums: action.albums};
        case SUCCESS_FETCH_TRACKS:
            return {...state, tracks: action.tracks};
        case SUCCESS_FETCH_HISTORY:
            return {...state, history: action.history};
        default:
            return state;
    }
};

export default reducer;